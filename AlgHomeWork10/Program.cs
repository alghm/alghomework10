﻿using System.Diagnostics;

internal class Program
{
    private static void Main(string[] args)
    {
        var tree1 = new BST();
        var tree2 = new BST();
        int N = 7000;
        int bigN = 10_000_000;
        var rnd = new Random();

        Console.WriteLine($"Tree1 - первое дерево - {N} чисел в случайном порядке");
        Console.WriteLine($"Tree2 - второе дерево - {N} чисел в возрастающем порядке");

        Console.WriteLine($"Сравнение производительности для двух деревьев, количество операций - { N / 10 }");

        Stopwatch stopwatch = new Stopwatch();

        stopwatch.Start();
        for (int i = 0; i < N; i++)
        {
            tree1.Insert(rnd.Next(1, N));
        }
        stopwatch.Stop();
        long elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Вставка для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N; i++)
        {
            tree2.Insert(i);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Вставка для второго дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            bool f = tree1.Search(rndNumber);
            //string isf = f ? "found" : "not found";
            //Console.WriteLine($"Number: {rndNumber}, in tree1 - {isf}");
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Поиск для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            bool f = tree2.Search(rndNumber);
            //string isf = f ? "found" : "not found";
            //Console.WriteLine($"Number: {rndNumber}, in tree2 - {isf}");
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Поиск для второго дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N + 1);
            tree1.Remove(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Удаление для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N + 1);
            tree2.Remove(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Удаление для второго дерева - {0}", elapsedTime);


        // bigN
        Console.WriteLine($"Для первого дерева с {bigN} элементами");
        stopwatch.Restart();
        for (int i = 0; i < bigN; i++)
        {
            tree1.Insert(rnd.Next(1, bigN));
        }
        stopwatch.Stop();
         elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Вставка для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < bigN / 10; i++)
        {
            int rndNumber = rnd.Next(1, bigN);
            bool f = tree1.Search(rndNumber);
            //string isf = f ? "found" : "not found";
            //Console.WriteLine($"Number: {rndNumber}, in tree1 - {isf}");
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Поиск для первого дерева - {0}", elapsedTime);

        stopwatch.Restart();
        for (int i = 0; i < bigN / 10; i++)
        {
            int rndNumber = rnd.Next(1, bigN + 1);
            tree1.Remove(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Удаление для первого дерева - {0}", elapsedTime);

        Console.ReadLine();
    }
}

public class Node
{
    public int Value;
    public Node Left, Right;

    public Node(int value)
    {
        this.Value = value;
        Left = Right = null;
    }
}

public class BST
{
    Node root;
    public BST()
    {
        root = null;
    }

    public void Insert(int node)
    {
        if (root == null)
            root = new Node(node);
        else
            InsertR(root, node);
    }

    void InsertR(Node root, int node)
    {
        if (node < root.Value)
        {
            if (root.Left == null)
            {
                root.Left = new Node(node);
            }
            else
            {
                InsertR(root.Left, node);
            }
        }
        else
        {
            if (root.Right == null)
            {
                root.Right = new Node(node);
            }
            else
            {
                InsertR(root.Right, node);
            }
        }
    }

    public bool Search(int node)
    {
        return SearchR(root, node);
    }

    bool SearchR(Node root, int node)
    {
        if (root == null)
        {
            return false;
        }

        if (root.Value == node)
        {
            return true;
        }

        if (root.Value < node)
        {
            return SearchR(root.Right, node);
        }

        return SearchR(root.Left, node);
    }

    public void Remove(int x)
    {
        root = RemoveR(root, x);
    }

    Node RemoveR(Node root, int node)
    {
        if (root == null)
        {
            return root;
        }

        if (node < root.Value)
        {
            root.Left = RemoveR(root.Left, node);
        }
        else if (node > root.Value)
        {
            root.Right = RemoveR(root.Right, node);
        }
        else
        {
            if (root.Left == null)
            {
                return root.Right;
            }

            if (root.Right == null)
            {
                return root.Left;
            }

            root.Value = GetMin(root.Right);
            root.Right = RemoveR(root.Right, root.Value);
        }

        return root;
    }

    static int GetMin(Node root)
    {
        int min = root.Value;
        while (root.Left != null)
        {
            min = root.Left.Value;
            root = root.Left;
        }
        return min;
    }
}
